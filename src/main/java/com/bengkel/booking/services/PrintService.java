package com.bengkel.booking.services;

import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Car;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;

public class PrintService {
	
	public static void printMenu(String[] listMenu, String title) {
		String line = "+---------------------------------+";
		int number = 1;
		String formatTable = " %-2s. %-25s %n";
		
		System.out.printf("%-25s %n", title);
		System.out.println(line);
		
		for (String data : listMenu) {
			if (number < listMenu.length) {
				System.out.printf(formatTable, number, data);
			}else {
				System.out.printf(formatTable, 0, data);
			}
			number++;
		}
		System.out.println(line);
		System.out.println();
	}
	
	public static void printVechicle(List<Vehicle> listVehicle) {
		String formatTable = "| %-2s | %-15s | %-10s | %-15s | %-15s | %-5s | %-15s |%n";
		String line = "+----+-----------------+------------+-----------------+-----------------+-------+-----------------+%n";
		System.out.format(line);
	    System.out.format(formatTable, "No", "Vechicle Id", "Warna", "Brand", "Transmisi", "Tahun", "Tipe Kendaraan");
	    System.out.format(line);
	    int number = 1;
	    String vehicleType = "";
	    for (Vehicle vehicle : listVehicle) {
	    	if (vehicle instanceof Car) {
				vehicleType = "Mobil";
			}else {
				vehicleType = "Motor";
			}
	    	System.out.format(formatTable, number, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getBrand(), vehicle.getTransmisionType(), vehicle.getYearRelease(), vehicleType);
	    	number++;
	    }
	    System.out.printf(line);
	}

	public static void printCustomerVehicles(Customer customer) {
		int num = 1;

		System.out.printf("| %4s | %15s | %10s | %15s |\n", "No", "Vechicle Id", "Warna", "Tipe Kendaraan", "Tahun");
		System.out.println("----------------------------------------------------");
		for (Vehicle vehicle : customer.getVehicles()) {
			System.out.printf("| %4d | %15s | %10s | %15s |\n", 
			num++, vehicle.getVehiclesId(), vehicle.getColor(), vehicle.getVehicleType(), vehicle.getYearRelease());
		}
	}

	public static void printUserInformations() {
		Customer userLogin = BengkelService.getUserLogin();

		System.out.println("--------------------------------------");
		System.out.println("Customer ID : " + userLogin.getCustomerId());
		System.out.println("Name : " + userLogin.getName());
		if (userLogin instanceof MemberCustomer) {
			System.out.println("Customer Status : Member");
			System.out.println("Saldo Koin : " + ((MemberCustomer) userLogin).getSaldoCoin());
		} else {
			System.out.println("Customer Status : Non Member");
		}
		System.out.println("Address : " + userLogin.getAddress());
		System.out.println("Vehicles List : "); 
		printCustomerVehicles(userLogin);
		System.out.println("--------------------------------------");
	}

	public static void printAllServices(List<ItemService> listService) {
		int num = 1;

		System.out.printf("| %4s | %15s | %15s | %15s |\n", "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga Service" );
		System.out.println("----------------------------------------------------");
		for (ItemService service : listService) {
			System.out.printf("| %4d | %15s | %15s | %15s |\n", 
			num++, service.getServiceId(), service.getServiceName(), service.getVehicleType(), service.getPrice());
		}
		System.out.println("----------------------------------------------------");
		System.out.println("0.Kembali ke Home Menu" );
	}

	public static void printSpecificServices(List<ItemService> listService, String serviceType) {
		int num = 1;

		System.out.printf("| %4s | %15s | %15s | %15s |\n", "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga Service" );
		System.out.println("----------------------------------------------------");
		for (ItemService service : listService) {
			if (service.getVehicleType().equals(serviceType)) {
				System.out.printf("| %4d | %15s | %15s | %15s |\n", 
				num++, service.getServiceId(), service.getServiceName(), service.getVehicleType(), service.getPrice());
			}
		}
		System.out.println("----------------------------------------------------");
		System.out.println("0.Kembali ke Home Menu" );
	}

	public static void printAllBookedServices(List<ItemService> listService) {
		int num = 1;

		System.out.printf("| %4s | %15s | %15s | %15s |\n", "No", "Service Id", "Nama Service", "Tipe Kendaraan", "Harga Service" );
		System.out.println("----------------------------------------------------");
		for (ItemService service : listService) {
			System.out.printf("| %4d | %15s | %15s | %15s |\n", 
			num++, service.getServiceId(), service.getServiceName(), service.getVehicleType(), service.getPrice());
		}
		System.out.println("----------------------------------------------------");
	}

	public static void printAllBookingOrder(List<BookingOrder> listBookingOrder) {
		int num = 1;
		String userId = BengkelService.getUserLogin().getCustomerId().substring(5);

		System.out.printf("| %4s | %15s | %15s | %15s | %15s | %15s | %15s |\n", "No", "Booking Id", "Nama Customer", "Payment Method", "Total Service", "Total Payment", "List Service" );
		System.out.println("----------------------------------------------------");
		for (BookingOrder order : listBookingOrder) {
			String allServices = "";
			StringBuilder sb = new StringBuilder(allServices);
			
			for (ItemService service : order.getServices()) {
				sb.append(service.getServiceName()).append(", ");
			}

			if (sb.length() > 0) {
				sb.setLength(sb.length() - 2); 
			}
			
			allServices = sb.toString();

			if (order.getBookingId().substring(14).equals(userId)) {
				System.out.printf("| %4s | %15s | %15s | %15s | %15s | %15s | %25s |\n", 
				num++, order.getBookingId(), order.getCustomer().getName(), order.getPaymentMethod(), order.getTotalServicePrice(), order.getTotalPayment(), allServices);
			}
		}
		System.out.println("----------------------------------------------------");
	}
	
	//Silahkan Tambahkan function print sesuai dengan kebutuhan.
	
}
