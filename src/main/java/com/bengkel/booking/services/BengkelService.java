package com.bengkel.booking.services;

import java.util.ArrayList;
import java.util.List;

import com.bengkel.booking.models.BookingOrder;
import com.bengkel.booking.models.Customer;
import com.bengkel.booking.models.ItemService;
import com.bengkel.booking.models.MemberCustomer;
import com.bengkel.booking.models.Vehicle;
import com.bengkel.booking.repositories.CustomerRepository;
import com.bengkel.booking.repositories.ItemServiceRepository;

public class BengkelService {

	private static List<Customer> listAllCustomers = CustomerRepository.getAllCustomer();
	private static List<ItemService> listAllItemService = ItemServiceRepository.getAllItemService();
	private static List<ItemService> listBookedServices = new ArrayList<>();
	private static List<BookingOrder> listBookingOrder = new ArrayList<>();

	private static Customer userLogin = null;
	private static List<String> userVehicles = new ArrayList<>();

	public static int loginCounter = 2;
	
	public static void login() {
		Customer selectedCustomer = null;
		
		do{
			String[] userCredential = Validation.userLoginValidation();

			selectedCustomer = listAllCustomers.stream()	
										.filter(customer -> customer.getCustomerId().equals(userCredential[0]) && customer.getPassword().equals(userCredential[1]))
										.findFirst().orElse(null);
										
			if (loginCounter > 0 && selectedCustomer == null) {
				System.out.println();
				System.out.println("User ID atau Password yang anda masukkan salah. Silahkan coba lagi!");
				loginCounter--;
			} else if (loginCounter <= 0 && selectedCustomer == null)  {
				System.out.println();
				System.out.println("Anda sudah melebihi batas mencoba login.");
				System.out.println("Exiting the program...");
				System.exit(0);
			}
		} while (selectedCustomer == null);

		if (selectedCustomer != null) {
			userLogin = selectedCustomer;
		}
	}

	public static void logout() {
		userLogin = null;
		userVehicles.clear();
		loginCounter = 2;
		MenuService.run();
	}

	// set user login
	public static Customer setUserLogin(Customer customer) {
		return customer;
	}

	// get user login
	public static Customer getUserLogin() {
		return userLogin;
	}

	//get listBookingOrder
	public static List<BookingOrder> getBookingOrderList() {
		return listBookingOrder;
	}

	public static void bookingService() {
		Vehicle selectedVehicle = selectingVehicle();
		if (selectedVehicle == null) {
			return;
		}
		
		System.out.println("List Service yang tersedia : ");
		System.out.println("----------------------------------------------------------");
		PrintService.printSpecificServices(listAllItemService, selectedVehicle.getVehicleType());
		System.err.println();
		
		List<ItemService> bookedServices = selectingServices();

		System.out.println("List Service yang anda booking : ");
		PrintService.printAllBookedServices(bookedServices);
		double totalServicePrice = bookedServices.stream()
								.mapToDouble(ItemService::getPrice)
								.sum();

		String paymentMethod = selectingPaymentMethod();

		int bookingId = listBookingOrder.size() + 1;
		String uniqueId = String.format("Book-Cust-%03d-%s", bookingId++, userLogin.getCustomerId().substring(5));

		BookingOrder bookingOrder = new BookingOrder(uniqueId, userLogin, bookedServices, paymentMethod, totalServicePrice);
		listBookingOrder.add(bookingOrder);

		if (paymentMethod.equals("Saldo Coin")) {
			double userSaldoCoin = ((MemberCustomer) userLogin).getSaldoCoin();

			if (userSaldoCoin < bookingOrder.getTotalPayment()) {
				System.out.println("Saldo Coin anda kurang! Pembayaran otomatis di ubah ke Cash.");

				BookingOrder previousBookingOrder = listBookingOrder.stream().filter(previous -> previous.getBookingId().equals(uniqueId)).findAny().orElse(bookingOrder);
				previousBookingOrder.setPaymentMethod("Cash");
			} else {
				((MemberCustomer) userLogin).setSaldoCoin(userSaldoCoin - bookingOrder.getTotalPayment());
			}
		}

		System.out.println("Total Harga Service : " + totalServicePrice);
		System.out.println("Total Pembayaran : " + bookingOrder.getTotalPayment());

	}

	public static Vehicle selectingVehicle() {
		System.out.println("List Vehicle yang tersedia : ");
		PrintService.printCustomerVehicles(userLogin);

		String vehicleId = Validation.validasiInput("Pilih Vehicle Id yang ingin di service : ", "Hanya huruf dan angka saja yang dapat dimasukkan","[a-zA-Z0-9]+");

		if (!userVehicles.contains(vehicleId)) {
			userVehicles.add(vehicleId);
		} else {
			System.out.println("Kendaraan sudah di pilih untuk di servis.");
			return null;
		}

		Vehicle selectedVehicle = userLogin.getVehicles().stream()
								.filter(vehicle -> vehicle.getVehiclesId().equals(vehicleId))
								.findFirst().orElse(null);

		if (selectedVehicle == null) {
			System.out.println("Kendaraan tidak ditemukan!");
			return null;
		}

		return selectedVehicle;
	}

	public static List<ItemService> selectingServices() {
		int maxNumberOfServices = userLogin.getMaxNumberOfService();
		List<ItemService> bookedServices = new ArrayList<>(listBookedServices);
		do {
			System.out.println();
			String serviceId = Validation.validasiInput("Masukkan Service ID : ", "Service Id tidak sesuai.","[a-zA-Z0-9-]+");
			
			ItemService selectedService = listAllItemService.stream()
										.filter(service -> service.getServiceId().equals(serviceId))
										.findFirst().orElse(null);

			if (selectedService == null) {
				System.out.println("Service Id tidak ditemukan.");
				continue;
			} else {
				bookedServices.add(selectedService);
				maxNumberOfServices--;
			}

			String confirmation;
			if (maxNumberOfServices > 0) {
				confirmation = Validation.validasiInput("Apakah anda ingin menambahkan service lainnya? (Y/T)", "Opsi tidak ada.", "(Y|T)");
			} else {
				confirmation = "T";
			}

			if (confirmation.equalsIgnoreCase("T")) {
				maxNumberOfServices = -1;
			}

		} while (maxNumberOfServices > 0);

		return bookedServices;
	}

	public static String selectingPaymentMethod() {
		String paymentMethod;
		if (userLogin instanceof MemberCustomer) {
			paymentMethod = Validation.validasiInput("Silahkan pilih Metode Pembayaran (Saldo Coin atau Cash) : ", "Opsi tidak ada.", "(Saldo Coin|Cash)");
		} else {
			paymentMethod = Validation.validasiInput("Silahkan pilih Metode Pembayaran (Cash) : ", "Opsi tidak ada.", "(Cash)");
		}

		return paymentMethod;
	}

	public static void topUpSaldoCoin(){
		if (userLogin instanceof MemberCustomer) {
			double userSaldoCoin = ((MemberCustomer) userLogin).getSaldoCoin();

			System.out.println("Saldo Coin anda sekarang : " + userSaldoCoin);
			double topUpSaldo = Double.valueOf(Validation.validasiInput("Masukkan Saldo Top-up : ", "Hanya dapat di isi dengan angka saja.", "[0-9]+"));

			((MemberCustomer) userLogin).setSaldoCoin(userSaldoCoin + topUpSaldo);
			System.out.println();
			System.out.println("Berhasil, Saldo anda sudah terisi!");

		} else {
			System.out.println("Maaf fitur ini hanya dapat digunakan untuk Member!");
		}
	}
	
}
