package com.bengkel.booking.services;

public class MenuService {

	public static void run() {
		boolean isLooping = true;
		do {
			if (BengkelService.getUserLogin() == null) {
				BengkelService.login();
			} else {
				MenuService.mainMenu();
			}
		} while (isLooping && BengkelService.loginCounter > 0);
		
	}

	public static void mainMenu() {
		String[] listMenu = {"Informasi Customer", "Booking Bengkel", "Top Up Bengkel Coin", "Informasi Booking", "Logout"};
		int menuChoice = 0;
		boolean isLooping = true;
		
		do {
			PrintService.printMenu(listMenu, "\nSelamat datang, " + BengkelService.getUserLogin().getName() + "!\nBooking Bengkel Menu");
			menuChoice = Validation.validasiNumberWithRange("Masukan Pilihan Menu:", "Input Harus Berupa Angka!", "^[0-9]+$", listMenu.length-1, 0);
			System.out.println(menuChoice);
			
			switch (menuChoice) {
			case 1:
				PrintService.printUserInformations();
				break;
			case 2:
				BengkelService.bookingService();
				break;
			case 3:
				BengkelService.topUpSaldoCoin();
				break;
			case 4:
				PrintService.printAllBookingOrder(BengkelService.getBookingOrderList());
				break;
			case 0:
				System.out.println("User Logout!");
				System.out.println();
				BengkelService.logout();
				break;
			}
		} while (isLooping);

	}

}
